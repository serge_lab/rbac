EXTENSION = rbac
PGFILEDESC = "rbac - role based access control implementation"

TESTS = $(wildcard test/*.sql)
REGRESS = $(patsubst test/%.sql,%,$(TESTS))

REGRESS_OPTS  = --load-extension=rbac \
		--load-extension=pgtap \
		--load-language=plpgsql

PG_CONFIG = pg_config
PG91 = $(shell $(PG_CONFIG) --version | egrep " 8\.| 9\.0" > /dev/null && echo no || echo yes)

DATA = $(filter-out $(wildcard sql/*--*.sql),$(wildcard sql/*.sql))
EXTVERSION = $(shell grep default_version $(EXTENSION).control | \
               sed -e "s/default_version[[:space:]]*=[[:space:]]*'\([^']*\)'/\1/")

ifeq ($(PG91),yes)
all: sql/$(EXTENSION)--$(EXTVERSION).sql

sql/$(EXTENSION)--$(EXTVERSION).sql: $(sort $(wildcard sql/tables.sql)) $(sort $(wildcard sql/functions.sql)) $(sort $(wildcard sql/tests.sql))
	cat $^ > $@

DATA = $(wildcard updates/*--*.sql) sql/$(EXTENSION)--$(EXTVERSION).sql
EXTRA_CLEAN = sql/$(EXTENSION)--$(EXTVERSION).sql
endif

PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

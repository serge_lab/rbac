-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION rbac" to load this file. \quit

/* This command creates a new RBAC user. The command is valid
 * only if the new user is not already a member of the USERS data set. The USER
 * data set is updated. The new user does not own any session at the time of its
 * creation.
 */
CREATE FUNCTION add_user(name text, title text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('create', 'user');

        INSERT INTO @extschema@.users VALUES(name, title);
        PERFORM @extschema@._audit(format('Create user %s', name));
        RETURN true;
EXCEPTION WHEN unique_violation THEN
        RAISE EXCEPTION E'User % already exists', name;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION add_user(text,text) IS 'This command creates a new RBAC user';

/* This command deletes an existing user from the RBAC database.
 * The command is valid if and only if the user to be deleted is a member of
 * the USERS data set. The USERS and UA data sets and the assigned users
 * function are updated. It is an implementation decision how to proceed with
 * the sessions owned by the user to be deleted. The RBAC system could wait
 * for such a session to terminate normally, or it could force its termination.
 */
CREATE FUNCTION delete_user(username text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('delete', 'user');

        DELETE FROM @extschema@.sessions WHERE user_name=username;
        DELETE FROM @extschema@.user_roles WHERE user_name=username;

        DELETE FROM @extschema@.users WHERE name=username;
        IF NOT found THEN
                RAISE EXCEPTION E'User % not found', username;
        END IF;
        PERFORM @extschema@._audit(format('Delete user %s', username));
        RETURN true;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION delete_user(text) IS 'This command deletes an existing user from the RBAC database';

/* This command creates a new role. The command is valid if and only
 * if the new role is not already a member of the ROLES data set. The ROLES data
 * set and the functions assigned users and assigned permissions are updated.
 * Initially, no user or permission is assigned to the new role.
 */
CREATE FUNCTION add_role(rolename text, title text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('create', 'role');

        INSERT INTO @extschema@.roles VALUES(rolename, title);
        PERFORM @extschema@._audit(format('Create role %s', rolename));
        RETURN true;
EXCEPTION WHEN unique_violation THEN
        RAISE EXCEPTION E'Role % already exists', rolename;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION add_role(text,text) IS ' This command creates a new role';

/* This command deletes an existing role from the RBAC database.
 * The command is valid if and only if the role to be deleted is a member of the
 * ROLES data set. It is an implementation decision how to proceed with the ses-
 * sions in which the role to be deleted is active. The RBAC system could wait
 * for such a session to terminate normally, it could force the termination of that
 * session, or it could delete the role from that session while allowing the ses-
 * sion to continue
 */
CREATE FUNCTION delete_role(rolename text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('delete', 'role');

        DELETE FROM @extschema@.roles WHERE name=rolename;
        IF NOT found THEN
                RAISE EXCEPTION E'Role % not found', rolename;
        END IF;
        PERFORM @extschema@._audit(format('Delete role %s', rolename));
        RETURN true;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION delete_role(text) IS 'This command deletes an existing role from the RBAC database';

/* This command assigns a user to a role. The command is valid if
 * and only if the user is a member of the USERS data set, the role is a member of
 * the ROLES data set, and the user is not already assigned to the role. The data
 * set UA and the function assigned users are updated to reflect the assignment.
 */
CREATE FUNCTION assign_user(username text, role text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('assign', 'user');

        PERFORM name FROM @extschema@.roles WHERE name=role;
        IF NOT found THEN
           RAISE EXCEPTION E'Role % not found', role;
        END IF;
        PERFORM name FROM @extschema@.users WHERE name=username;
        IF NOT found THEN
           RAISE EXCEPTION E'User % not found', username;
        END IF;
        PERFORM role_name FROM @extschema@.user_roles WHERE role_name=role AND user_name=username;
        IF found THEN
           RAISE EXCEPTION E'Role % already assigned to user %', role, username;
        END IF;
        INSERT INTO @extschema@.user_roles VALUES(username, role);
        PERFORM rbac._audit(format('Assign user %s to role %s', username, role));
        RETURN true;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION assign_user(text, text) IS 'This command assigns a user to a role';

/* This command deletes the assignment of the user user to the
 * role role. The command is valid if and only if the user is a member of the USERS
 * data set, the role is a member of the ROLES data set, and the user is assigned
 * to the role. It is an implementation decision how to proceed with the sessions
 * in which the session user is user and one of his or her active roles is role. The
 * RBAC system could wait for such a session to terminate normally, could force its
 * termination, or could inactivate the role. Our presentation illustrates the case
 * when those sessions are forcefully terminated.
 */
CREATE FUNCTION deassign_user(username text, role text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('deassign', 'user');

        PERFORM name FROM @extschema@.roles WHERE name=role;
        IF NOT found THEN
           RAISE EXCEPTION E'Role % not found', role;
        END IF;
        PERFORM name FROM @extschema@.users WHERE name=username;
        IF NOT found THEN
           RAISE EXCEPTION E'User % not found', username;
        END IF;
        PERFORM role_name FROM @extschema@.user_roles WHERE role_name=role AND user_name=username;
        IF NOT found THEN
           RAISE EXCEPTION E'Role % already deassigned to user %', role, username;
        END IF;
        DELETE FROM @extschema@.user_roles WHERE user_name=username and role_name=role;
        PERFORM @extschema@._audit(format('Deassign user %s from role %s', username, role));
        RETURN true;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION deassign_user(text, text) IS 'This command deletes the assignment of the user user to the role role';

/* This command grants a role the permission to perform an
 * operation on an object to a role. The command may be implemented as granting
 * permissions to a group corresponding to that role, that is, setting the access
 * control list of the object involved. The command is valid if and only if the pair
 * (operation, object) represents a permission, and the role is a member of the
 * ROLES data set.
 */
CREATE FUNCTION grant_permission(operation text, object text, role text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('grant', 'permission');

        PERFORM name FROM @extschema@.roles WHERE name=role;
        IF NOT found THEN
           RAISE EXCEPTION E'Role % not found', role;
        END IF;
        PERFORM op_name FROM @extschema@.permissions WHERE op_name=operation AND obj_name=object;
        IF NOT found THEN
           RAISE EXCEPTION E'Permission (%,%) not found', operation, object;
        END IF;
        PERFORM role_name FROM @extschema@.role_permissions WHERE role_name=role AND op_name=operation AND obj_name=object;
        IF found THEN
           RAISE EXCEPTION E'Permission (%,%) already granted to role %', operation, object, role;
        END IF;
        INSERT INTO @extschema@.role_permissions VALUES(role, object, operation);
        PERFORM @extschema@._audit(format('Grant permission (%s,%s) to role %s', object, operation, role));
        RETURN true;
EXCEPTION WHEN unique_violation THEN
        RAISE EXCEPTION E'Permission (%,%) already exists', operation, object;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION grant_permission(text, text, text) IS 'This command grants a role the permission to perform an operation on an object to a role';

/* This command revokes the permission to perform an op-
 * eration on an object from the set of permissions assigned to a role. The command
 * may be implemented as revoking permissions from a group corresponding to
 * that role, that is, setting the access control list of the object involved. The com-
 * mand is valid if and only if the pair (operation, object) represents a permission,
 * the role is a member of the ROLES data set, and the permission is assigned to
 * that role.
 */
CREATE FUNCTION revoke_permission(operation text, object text, role text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('revoke', 'permission');

        PERFORM name FROM @extschema@.roles WHERE name=role;
        IF NOT found THEN
           RAISE EXCEPTION E'Role % not found', role;
        END IF;
        PERFORM op_name FROM @extschema@.permissions WHERE op_name=operation AND obj_name=object;
        IF NOT found THEN
           RAISE EXCEPTION E'Permission (%,%) not found', operation, object;
        END IF;
        PERFORM role_name FROM @extschema@.role_permissions WHERE role_name=role AND op_name=operation AND obj_name=object;
        IF NOT found THEN
           RAISE EXCEPTION E'Permission (%,%) already revoked from role %', operation, object, role;
        END IF;
        DELETE FROM @extschema@.role_permissions WHERE role_name=role AND obj_name=object AND op_name=operation;
        PERFORM @extschema@._audit(format('Revoke permission (%s,%s) from role %s', object, operation, role));
        RETURN true;
EXCEPTION WHEN OTHERS THEN
        RAISE EXCEPTION E'Internal error';
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION grant_permission(text, text, text) IS 'This command revokes the permission to perform an operation on an object from the set of permissions assigned to a role';

-- System functions

/* This function creates a new session with a given
 * user as owner and an active role set. The function is valid if and only if:
 * —the user is a member of the USERS data set, and
 * —the active role set is a subset of the roles assigned to that user. In an RBAC
 * implementation, the session’s active roles might actually be the groups that
 * represent those roles.
 */
CREATE FUNCTION create_session (username text, roles text[] DEFAULT array[]::text[]) RETURNS uuid AS $$
DECLARE
        sid uuid;
BEGIN
        PERFORM name FROM @extschema@.users WHERE name=username;
        IF NOT found THEN
           RAISE EXCEPTION E'User % not found', username;
        END IF;

        IF NOT (SELECT array_agg(role_name) FROM @extschema@.user_roles WHERE user_name=username) @> roles THEN
           RAISE EXCEPTION E'Roles % not in user % roleset', roles, username;
        END IF;

        sid := gen_random_uuid();
        INSERT INTO @extschema@.sessions VALUES(sid, username, roles);
        PERFORM set_config('@extschema@.session_id', sid::text, false);
        PERFORM @extschema@._audit(format('Create session %s', sid::text));

        RETURN sid;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION create_session (text, text[]) IS 'This function creates a new session with a given user as owner and an active role set';

/* This function deletes a given session with a
 * given owner user. The function is valid if and only if the session identifier is a
 * member of the SESSIONS data set, the user is a member of the USERS data
 * set, and the session is owned by the given user.
 */
CREATE FUNCTION delete_session(username text, session uuid) RETURNS boolean AS $$
BEGIN
        PERFORM id FROM @extschema@.sessions WHERE user_name=username AND id=session;
        IF NOT found THEN
           RAISE EXCEPTION E'User % session % not found', username, session;
        END IF;

        PERFORM @extschema@._audit(format('Delete session %s', session));
        DELETE FROM @extschema@.sessions WHERE user_name=username AND id=session;
        RETURN true;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION delete_session (text, uuid) IS 'This function deletes a given session with a given owner user';

/* This function adds a role as an active role of a session whose
 * owner is a given user. The function is valid if and only if:
 * —the user is a member of the USERS data set,
 * —the role is a member of the ROLES data set,
 * —the session identifier is a member of the SESSIONS data set,
 * —the role is assigned to the user, and
 * —the session is owned by that user.
 */
CREATE FUNCTION add_active_role(username text, session uuid, role text) RETURNS boolean AS $$
DECLARE
        _active_roles text[];
BEGIN
        PERFORM user_name FROM @extschema@.user_roles WHERE user_name=username AND role_name=role;
        IF NOT found THEN
           RAISE NOTICE E'Role % not assigned to user %', role, username;
           RETURN false;
        END IF;

        SELECT active_roles INTO _active_roles FROM @extschema@.sessions WHERE user_name=username AND id=session;
        IF NOT found THEN
           RAISE NOTICE E'User % session % not found', username, session;
           RETURN false;
        END IF;

        IF _active_roles && ARRAY[role] THEN
           RETURN true;
        END IF;

        UPDATE @extschema@.sessions SET active_roles = array_append(active_roles, role) WHERE id=session;
        PERFORM @extschema@._audit(format('Add role %s to session %s', role, session));
        RETURN true;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION add_active_role(text, uuid, text) IS 'This function deletes a role from the active role set of a session owned by a given user';

/* This function deletes a role from the active role set of a
 * session owned by a given user. The function is valid if and only if the user is
 * a member of the USERS data set, the session identifier is a member of the
 * SESSIONS data set, the session is owned by the user, and the role is an active
 * role of that session.
 */
CREATE FUNCTION drop_active_role(username text, session uuid, role text) RETURNS boolean AS $$
DECLARE
        _active_roles text[];
BEGIN
        SELECT active_roles INTO _active_roles FROM @extschema@.sessions WHERE user_name=username AND id=session;
        IF NOT found THEN
           RAISE NOTICE E'User % session % not found', username, session;
           RETURN false;
        END IF;

        IF NOT _active_roles && ARRAY[role] THEN
           RAISE NOTICE E'Role % not found in session %', role, session;
           RETURN true;
        END IF;

        UPDATE @extschema@.sessions SET active_roles = array_remove(active_roles, role) WHERE id=session;
        PERFORM @extschema@._audit(format('Drop role %s from session %s', role, session));
        RETURN true;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION drop_active_role(text, uuid, text) IS 'This function deletes a role from the active role set of a session owned by a given user';

--- This function returns a Boolean value meaning the subject of
--- a given session is or is not allowed to perform a given operation on a given
--- object. The function is valid if and only if the session identifier is a member
--- of the SESSIONS data set, the object is a member of the OBJS data set, and
--- the operation is a member of the OPS data set. The session’s subject has the
--- permission to perform the operation on that object if and only if that permis-
--- sion is assigned to (at least) one of the session’s active roles. An implementation
--- might use the groups that correspond to the subject’s active roles and their per-
--- missions as registered in the object’s access control list.
CREATE FUNCTION check_access (session uuid, operation text, object text) RETURNS boolean AS $$
BEGIN
        IF session IS NULL THEN
           session := current_setting('@extschema@.session_id');
        END IF;

        PERFORM role_name
        FROM @extschema@.role_permissions
        INNER JOIN @extschema@.sessions
              ON @extschema@.sessions.active_roles && ARRAY[@extschema@.role_permissions.role_name]
              AND @extschema@.sessions.id=session
        WHERE op_name=operation
        AND obj_name=object;

        RETURN found;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION check_access (uuid, text, text) IS 'This function returns a Boolean value meaning the subject of a given session is or is not allowed to perform a given operation on a given object';

-- Review functions

--- This function returns the set of users assigned to a given
--- role. The function is valid if and only if the role is a member of the ROLES data
--- set.
CREATE FUNCTION assigned_users (role text) RETURNS SETOF users AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('view', 'user');
        RETURN QUERY SELECT @extschema@.users.*
               FROM @extschema@.users
               INNER JOIN @extschema@.user_roles
                     ON @extschema@.user_roles.user_name = @extschema@.users.name
                     AND @extschema@.user_roles.role_name=role;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION assigned_users(text) IS 'This function returns the set of users assigned to a given role';

--- This function returns the set of roles assigned to a given user.
--- The function is valid if and only if the user is a member of the USERS data set.
CREATE FUNCTION assigned_roles (username text) RETURNS SETOF roles AS $$
BEGIN
        -- PERFORM @extschema@._is_granted_for('view', 'role');
        RETURN QUERY SELECT @extschema@.roles.*
               FROM @extschema@.roles
               INNER JOIN @extschema@.user_roles
                     ON @extschema@.user_roles.user_name = username
                     AND @extschema@.user_roles.role_name=@extschema@.roles.name;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION assigned_roles (text) IS 'This function returns the set of roles assigned to a given user';

--- This function returns the set of permissions (op, obj) gran-
--- ted to a given role. The function is valid if and only if the role is a member of
--- the ROLES data set.
CREATE FUNCTION role_permissions (role text) RETURNS SETOF permissions AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('view', 'permission');
        RETURN QUERY SELECT @extschema@.permissions.*
               FROM @extschema@.permissions
               INNER JOIN @extschema@.role_permissions
                     ON @extschema@.role_permissions.op_name = @extschema@.permissions.op_name
                     AND @extschema@.role_permissions.obj_name = @extschema@.permissions.obj_name
                     AND @extschema@.role_permissions.role_name=role;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION role_permissions (text) IS 'This function returns the set of permissions (op, obj) granted to a given role';

--- This function returns the permissions a given user gets
--- through his or her assigned roles. The function is valid if and only if the user
--- is a member of the USERS data set.
CREATE FUNCTION user_permissions (username text) RETURNS SETOF permissions AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('view', 'permission');
        RETURN QUERY SELECT @extschema@.permissions.*
               FROM @extschema@.permissions
               INNER JOIN @extschema@.role_permissions
                     ON @extschema@.role_permissions.op_name = @extschema@.permissions.op_name
                     AND @extschema@.role_permissions.obj_name = @extschema@.permissions.obj_name
               INNER JOIN @extschema@.user_roles
                     ON @extschema@.user_roles.role_name = @extschema@.role_permissions.role_name
                     AND @extschema@.user_roles.user_name=username;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION user_permissions (text) IS ' This function returns the permissions a given user gets through his or her assigned roles';

--- This function returns the active roles associated with a ses-
--- sion. The function is valid if and only if the session identifier is a mem-
--- ber of the SESSIONS data set.
CREATE FUNCTION session_roles (session uuid) RETURNS SETOF roles AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('view', 'role');
        RETURN QUERY SELECT @extschema@.roles.*
               FROM @extschema@.roles
               INNER JOIN @extschema@.sessions
                     ON @extschema@.sessions.active_roles && ARRAY[@extschema@.roles.name]
                     AND @extschema@.sessions.id = session;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION session_roles (uuid) IS 'This function returns the active roles associated with a session';

--- This function returns the permissions of the session
--- session, that is, the permissions assigned to its active roles. The function is
--- valid if and only if the session identifier is a member of the SESSIONS data
--- set.
CREATE FUNCTION session_permissions (session uuid) RETURNS SETOF permissions AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('view', 'permission');
        RETURN QUERY SELECT @extschema@.permissions.*
               FROM @extschema@.permissions
               INNER JOIN @extschema@.role_permissions
                     ON @extschema@.role_permissions.op_name = @extschema@.permissions.op_name
                     AND @extschema@.role_permissions.obj_name = @extschema@.permissions.obj_name
               INNER JOIN @extschema@.sessions
                     ON @extschema@.sessions.active_roles && ARRAY[@extschema@.role_permissions.role_name]
                     AND @extschema@.sessions.id = session;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION session_permissions (uuid) IS 'This function returns the permissions of the session session, that is, the permissions assigned to its active roles';

--- This function returns the set of operations a given
--- role is permitted to perform on a given object. The function is valid only if the
--- role is a member of the ROLES data set, and the object is a member of the
--- OBJS data set.
CREATE FUNCTION role_operations_on_object (role text, object text) RETURNS SETOF operations AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('view', 'operation');
        RETURN QUERY SELECT @extschema@.operations.*
               FROM @extschema@.operations
               INNER JOIN @extschema@.role_permissions
                     ON @extschema@.role_permissions.op_name = @extschema@.operations.name
                     AND @extschema@.role_permissions.obj_name = object
                     AND @extschema@.role_permissions.role_name = role;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION role_operations_on_object (text, text) IS 'This function returns the set of operations a given role is permitted to perform on a given object';

--- This function returns the set of operations a given
--- user is permitted to perform on a given object, obtained either directly or
--- through his or her assigned roles. The function is valid if and only if the user is
--- a member of the USERS data set and the object is a member of the OBJS data
--- set.
CREATE FUNCTION user_operations_on_object (username text, object text) RETURNS SETOF operations AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('view', 'operation');
        RETURN QUERY SELECT @extschema@.operations.*
               FROM @extschema@.operations
               INNER JOIN @extschema@.role_permissions
                     ON @extschema@.role_permissions.op_name = @extschema@.operations.name
                     AND @extschema@.role_permissions.obj_name = object
               INNER JOIN @extschema@.user_roles
                     ON @extschema@.user_roles.role_name = @extschema@.role_permissions.role_name;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION user_operations_on_object (text, text) IS 'This function returns the set of operations a given user is permitted to perform on a given object, obtained either directly or through his or her assigned roles';

-- Internal functions
CREATE FUNCTION _is_granted_for(operation text, object text) RETURNS void AS $$
BEGIN
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION _audit(action text) RETURNS void AS $$
BEGIN
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION _add_object(name text, title text) RETURNS boolean AS $$
BEGIN
        INSERT INTO @extschema@.objects VALUES(name, title);
        RETURN true;
EXCEPTION WHEN unique_violation THEN
        RAISE NOTICE E'Object % already exists', name;
        RETURN false;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION _add_object(text,text) IS 'This command creates a new RBAC object';

CREATE FUNCTION _add_operation(name text, title text) RETURNS boolean AS $$
BEGIN
        INSERT INTO @extschema@.operations VALUES(name, title);
        RETURN true;
EXCEPTION WHEN unique_violation THEN
        RAISE NOTICE E'Operation % already exists', name;
        RETURN false;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION _add_operation(text,text) IS 'This command creates a new RBAC operation';

CREATE FUNCTION _add_permission(object text, operation text, title text) RETURNS boolean AS $$
BEGIN
        INSERT INTO @extschema@.permissions VALUES(object, operation, title);
        RETURN true;
EXCEPTION WHEN unique_violation THEN
        RAISE NOTICE E'Permission (%,%) already exists', object, operation;
        RETURN false;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION _add_permission(text,text,text) IS 'This command creates a new RBAC permission';

-- Fixtures

SELECT _add_object('object', 'rbac_object_object');
SELECT _add_object('operation', 'rbac_operation_object');
SELECT _add_object('permission', 'rbac_permission_object');
SELECT _add_object('role', 'rbac_role_object');
SELECT _add_object('user', 'rbac_user_object');

SELECT _add_operation('create', 'rbac_create_operation');
SELECT _add_operation('edit', 'rbac_edit_operation');
SELECT _add_operation('delete', 'rbac_delete_operation');
SELECT _add_operation('view', 'rbac_view_operation');
SELECT _add_operation('assign', 'rbac_assign_operation');
SELECT _add_operation('deassign', 'rac_deassign_operation');
SELECT _add_operation('grant', 'rbac_grant_operation');
SELECT _add_operation('revoke', 'rac_revoke_operation');

SELECT _add_permission('object', 'view', 'rbac_view_object_permission');
SELECT _add_permission('operation', 'view', 'rbac_view_operation_permission');

SELECT _add_permission('permission', 'grant', 'rbac_grant_permission_permission');
SELECT _add_permission('permission', 'revoke', 'rbac_revoke_permission_permission');
SELECT _add_permission('permission', 'view', 'rbac_view_permission_permission');

SELECT _add_permission('role', 'create', 'rbac_create_role_permission');
SELECT _add_permission('role', 'delete', 'rbac_delete_role_permission');
SELECT _add_permission('role', 'view', 'rbac_view_role_permission');

SELECT _add_permission('user', 'create', 'rbac_create_user_permission');
SELECT _add_permission('user', 'delete', 'rbac_delete_user_permission');
SELECT _add_permission('user', 'view', 'rbac_view_user_permission');
SELECT _add_permission('user', 'assign', 'rbac_grant_role_permission');
SELECT _add_permission('user', 'deassign', 'rbac_revoke_role_permission');

SELECT add_user('superuser', 'RBAC schema and user administrator');

SELECT add_role('rbac_schema', 'RBAC schema administrator');
SELECT add_role('rbac_users', 'RBAC user administrator');

SELECT grant_permission('grant', 'permission', 'rbac_schema');
SELECT grant_permission('revoke', 'permission', 'rbac_schema');
SELECT grant_permission('view', 'permission', 'rbac_schema');
SELECT grant_permission('view', 'object', 'rbac_schema');
SELECT grant_permission('view', 'operation', 'rbac_schema');

SELECT grant_permission('create', 'role', 'rbac_schema');
SELECT grant_permission('delete', 'role', 'rbac_schema');
SELECT grant_permission('view', 'role', 'rbac_schema');

SELECT grant_permission('create', 'user', 'rbac_users');
SELECT grant_permission('delete', 'user', 'rbac_users');
SELECT grant_permission('view', 'user', 'rbac_users');
SELECT grant_permission('assign', 'user', 'rbac_users');
SELECT grant_permission('deassign', 'user', 'rbac_users');
SELECT grant_permission('view', 'role', 'rbac_users');
SELECT grant_permission('view', 'permission', 'rbac_users');
SELECT grant_permission('view', 'object', 'rbac_users');
SELECT grant_permission('view', 'operation', 'rbac_users');

SELECT assign_user('superuser', 'rbac_schema');
SELECT assign_user('superuser', 'rbac_users');

-- Setup internal check_access and audit

CREATE OR REPLACE FUNCTION _audit(action text) RETURNS void AS $$
DECLARE
        created_by text;
BEGIN
        SELECT user_name INTO created_by FROM @extschema@.sessions WHERE id = current_setting('@extschema@.session_id')::uuid;
        INSERT INTO @extschema@.audit(action, created_by) VALUES(action, created_by);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION _is_granted_for(operation text, object text) RETURNS void AS $$
DECLARE
        sid uuid;
BEGIN
        sid := current_setting('@extschema@.session_id');

        IF NOT @extschema@.check_access(sid, operation, object) THEN
           RAISE EXCEPTION 'Permission denied';
        END IF;
END;
$$ LANGUAGE plpgsql;

-- Tests

CREATE OR REPLACE FUNCTION @extschema@.testschema()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
    RETURN NEXT has_table( '@extschema@'::name, 'objects'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'operations'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'permissions'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'users'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'roles'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'role_permissions'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'user_roles'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'sessions'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'session_roles'::name );
END;
$$;

CREATE OR REPLACE FUNCTION @extschema@.testfunctions()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
    RETURN NEXT has_function( '@extschema@'::name, 'add_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'delete_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'add_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'delete_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'grant_permission'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'revoke_permission'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'assign_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'deassign_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'create_session'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'delete_session'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'add_active_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'drop_active_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'check_access'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'assigned_users'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'assigned_roles'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'role_permissions'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'user_permissions'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'session_roles'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'session_permissions'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'user_operations_on_object'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'role_operations_on_object'::name );
END;
$$;

CREATE OR REPLACE FUNCTION @extschema@.testreviewfunc()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
        PERFORM @extschema@.create_session('superuser');
        PERFORM @extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema');
        PERFORM @extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users');

        RETURN NEXT set_eq('SELECT * FROM @extschema@.assigned_users(''rbac_schema'')', 'SELECT * FROM @extschema@.users', 'check assigned users');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.assigned_roles(''superuser'')', 'SELECT * FROM @extschema@.roles', 'check assigned roles');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.user_permissions(''superuser'')', 'SELECT * FROM @extschema@.permissions', 'check user permissions');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.role_permissions(''rbac_users'')', 'SELECT * FROM @extschema@.permissions WHERE obj_name=''user'' UNION SELECT * FROM @extschema@.permissions WHERE op_name=''view''', 'check role permissions');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles', 'check session roles');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_permissions(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.permissions', 'check session permissions');

        PERFORM @extschema@.drop_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles WHERE name = ''rbac_users''', 'check rbac_users session roles');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_permissions(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.permissions WHERE obj_name=''user'' UNION SELECT * FROM @extschema@.permissions WHERE op_name=''view''', 'check rbac_users session permissions');

        RETURN NEXT set_eq('SELECT * FROM @extschema@.role_operations_on_object(''rbac_schema'', ''role'')', 'SELECT * FROM @extschema@.operations WHERE name IN (''create'', ''delete'', ''view'')', 'check role operations on object');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.user_operations_on_object(''superuser'', ''role'')', 'SELECT * FROM @extschema@.operations WHERE name IN (''create'', ''delete'', ''view'')', 'check user operations on object');
END;
$$;

CREATE OR REPLACE FUNCTION @extschema@.testsystemfunc()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
        RETURN NEXT isa_ok(@extschema@.create_session('superuser'), 'uuid', 'session created');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema'), true, 'add active role rbac_schema');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles WHERE name = ''rbac_schema''', 'session roles 1');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users'), true, 'add active role rbac_user');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles', 'session roles 2');
        RETURN NEXT is(@extschema@.drop_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema'), true, 'drop active role rbac_schema');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles WHERE name = ''rbac_users''', 'session roles 3');
        RETURN NEXT is(@extschema@.drop_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users'), true, 'drop active role rbac_schema');
        RETURN NEXT throws_ok('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'P0001', 'Permission denied');
        RETURN NEXT is(@extschema@.delete_session('superuser', current_setting('@extschema@.session_id')::uuid), true, 'session created');
END;
$$;


CREATE OR REPLACE FUNCTION @extschema@.testcorefunc()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
        RETURN NEXT isa_ok(@extschema@.create_session('superuser'), 'uuid', 'session created');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema'), true, 'add active role rbac_schema');

        RETURN NEXT throws_ok('SELECT * FROM @extschema@.add_role(''rbac_schema'', ''test'')', 'P0001', 'Role rbac_schema already exists');
        RETURN NEXT is(@extschema@.add_role('test_role', 'test'), 't', 'add role');

        RETURN NEXT throws_ok('SELECT @extschema@.assign_user(''superuser'', ''no_role'')', 'P0001', 'Permission denied');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users'), 't', 'add active role');
        RETURN NEXT throws_ok('SELECT * FROM @extschema@.assign_user(''superuser'', ''no_role'')', 'P0001', 'Role no_role not found');
        RETURN NEXT is(@extschema@.assign_user('superuser', 'test_role'), 't', 'role assigned');

        RETURN NEXT is(@extschema@.deassign_user('superuser', 'test_role'), 't', 'delete role');

        RETURN NEXT is(@extschema@.delete_role('test_role'), 't', 'delete role');
        RETURN NEXT throws_ok('SELECT * FROM @extschema@.delete_role(''test_role'')', 'P0001', 'Role test_role not found');
END;
$$;

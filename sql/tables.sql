-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION rbac" to load this file. \quit

CREATE TABLE objects (
       name text not null primary key,
       title text
);
COMMENT ON TABLE objects IS 'RBAC objects set';
SELECT pg_catalog.pg_extension_config_dump('objects', '');

CREATE TABLE operations (
       name text not null primary key,
       title text
);
COMMENT ON TABLE operations IS 'RBAC operations set';
SELECT pg_catalog.pg_extension_config_dump('operations', '');

CREATE TABLE permissions (
       obj_name text not null,
       op_name text not null,
       title text not null,
       PRIMARY KEY(obj_name, op_name),
       FOREIGN KEY (obj_name) REFERENCES objects(name) DEFERRABLE,
       FOREIGN KEY (op_name) REFERENCES operations(name) DEFERRABLE
);
COMMENT ON TABLE permissions IS 'RBAC permissions (object, operations) set';
SELECT pg_catalog.pg_extension_config_dump('permissions', '');

CREATE TABLE "roles" (
       name text not null primary key,
       title text not null
);
COMMENT ON TABLE roles IS 'RBAC roles set';
SELECT pg_catalog.pg_extension_config_dump('roles', '');

CREATE TABLE role_permissions (
       role_name text not null,
       obj_name text not null,
       op_name text not null,
       PRIMARY KEY (role_name, obj_name, op_name),
       FOREIGN KEY (role_name) REFERENCES roles(name) DEFERRABLE,
       FOREIGN KEY (obj_name, op_name) REFERENCES permissions(obj_name, op_name) DEFERRABLE
);
COMMENT ON TABLE role_permissions IS 'RBAC role permission set';
SELECT pg_catalog.pg_extension_config_dump('role_permissions', '');

CREATE TABLE "users" (
       name text not null primary key,
       title text not null
);
COMMENT ON TABLE users IS 'RBAC users set';
SELECT pg_catalog.pg_extension_config_dump('users', '');

CREATE TABLE user_roles (
       user_name text not null,
       role_name text not null,
       PRIMARY KEY (user_name, role_name),
       FOREIGN KEY (user_name) REFERENCES users(name) DEFERRABLE,
       FOREIGN KEY (role_name) REFERENCES roles(name) DEFERRABLE
);
COMMENT ON TABLE user_roles IS 'RBAC user roles set';
SELECT pg_catalog.pg_extension_config_dump('user_roles', '');

CREATE TABLE sessions (
       id uuid not null primary key,
       user_name text not null,
       active_roles text[],
       FOREIGN KEY (user_name) REFERENCES users(name) DEFERRABLE
);
COMMENT ON TABLE sessions IS 'RBAC sessions set';
SELECT pg_catalog.pg_extension_config_dump('sessions', '');

CREATE TABLE session_roles (
       session_id uuid not null,
       role_name text not null,
       FOREIGN KEY (session_id) REFERENCES sessions(id) DEFERRABLE,
       FOREIGN KEY (role_name) REFERENCES roles(name) DEFERRABLE
);
COMMENT ON TABLE session_roles IS 'RBAC session roles set';
SELECT pg_catalog.pg_extension_config_dump('session_roles', '');

CREATE TABLE audit (
       action text not null,
       created_by text not null,
       created_at timestamp not null default now()
);
COMMENT ON TABLE audit IS 'RBAC event log';

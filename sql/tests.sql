-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION rbac" to load this file. \quit

-- Tests

CREATE OR REPLACE FUNCTION @extschema@.testschema()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
    RETURN NEXT has_table( '@extschema@'::name, 'objects'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'operations'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'permissions'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'users'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'roles'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'role_permissions'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'user_roles'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'sessions'::name );
    RETURN NEXT has_table( '@extschema@'::name, 'session_roles'::name );
END;
$$;

CREATE OR REPLACE FUNCTION @extschema@.testfunctions()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
    RETURN NEXT has_function( '@extschema@'::name, 'add_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'delete_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'add_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'delete_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'grant_permission'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'revoke_permission'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'assign_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'deassign_user'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'create_session'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'delete_session'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'add_active_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'drop_active_role'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'check_access'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'assigned_users'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'assigned_roles'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'role_permissions'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'user_permissions'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'session_roles'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'session_permissions'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'user_operations_on_object'::name );
    RETURN NEXT has_function( '@extschema@'::name, 'role_operations_on_object'::name );
END;
$$;

CREATE OR REPLACE FUNCTION @extschema@.testreviewfunc()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
        PERFORM @extschema@.create_session('superuser');
        PERFORM @extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema');
        PERFORM @extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users');

        RETURN NEXT set_eq('SELECT * FROM @extschema@.assigned_users(''rbac_schema'')', 'SELECT * FROM @extschema@.users', 'check assigned users');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.assigned_roles(''superuser'')', 'SELECT * FROM @extschema@.roles', 'check assigned roles');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.user_permissions(''superuser'')', 'SELECT * FROM @extschema@.permissions', 'check user permissions');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.role_permissions(''rbac_users'')', 'SELECT * FROM @extschema@.permissions WHERE obj_name=''user'' UNION SELECT * FROM @extschema@.permissions WHERE op_name=''view''', 'check role permissions');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles', 'check session roles');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_permissions(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.permissions', 'check session permissions');

        PERFORM @extschema@.drop_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles WHERE name = ''rbac_users''', 'check rbac_users session roles');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_permissions(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.permissions WHERE obj_name=''user'' UNION SELECT * FROM @extschema@.permissions WHERE op_name=''view''', 'check rbac_users session permissions');

        RETURN NEXT set_eq('SELECT * FROM @extschema@.role_operations_on_object(''rbac_schema'', ''role'')', 'SELECT * FROM @extschema@.operations WHERE name IN (''create'', ''delete'', ''view'')', 'check role operations on object');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.user_operations_on_object(''superuser'', ''role'')', 'SELECT * FROM @extschema@.operations WHERE name IN (''create'', ''delete'', ''view'')', 'check user operations on object');
END;
$$;

CREATE OR REPLACE FUNCTION @extschema@.testsystemfunc()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
        RETURN NEXT isa_ok(@extschema@.create_session('superuser'), 'uuid', 'session created');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema'), true, 'add active role rbac_schema');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles WHERE name = ''rbac_schema''', 'session roles 1');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users'), true, 'add active role rbac_user');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles', 'session roles 2');
        RETURN NEXT is(@extschema@.drop_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema'), true, 'drop active role rbac_schema');
        RETURN NEXT set_eq('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'SELECT * FROM @extschema@.roles WHERE name = ''rbac_users''', 'session roles 3');
        RETURN NEXT is(@extschema@.drop_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users'), true, 'drop active role rbac_schema');
        RETURN NEXT throws_ok('SELECT * FROM @extschema@.session_roles(current_setting(''@extschema@.session_id'')::uuid)', 'P0001', 'Permission denied');
        RETURN NEXT is(@extschema@.delete_session('superuser', current_setting('@extschema@.session_id')::uuid), true, 'session created');
END;
$$;


CREATE OR REPLACE FUNCTION @extschema@.testcorefunc()
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
        RETURN NEXT isa_ok(@extschema@.create_session('superuser'), 'uuid', 'session created');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_schema'), true, 'add active role rbac_schema');

        RETURN NEXT throws_ok('SELECT * FROM @extschema@.add_role(''rbac_schema'', ''test'')', 'P0001', 'Role rbac_schema already exists');
        RETURN NEXT is(@extschema@.add_role('test_role', 'test'), 't', 'add role');

        RETURN NEXT throws_ok('SELECT @extschema@.assign_user(''superuser'', ''no_role'')', 'P0001', 'Permission denied');
        RETURN NEXT is(@extschema@.add_active_role('superuser', current_setting('@extschema@.session_id')::uuid, 'rbac_users'), 't', 'add active role');
        RETURN NEXT throws_ok('SELECT * FROM @extschema@.assign_user(''superuser'', ''no_role'')', 'P0001', 'Role no_role not found');
        RETURN NEXT is(@extschema@.assign_user('superuser', 'test_role'), 't', 'role assigned');

        RETURN NEXT is(@extschema@.deassign_user('superuser', 'test_role'), 't', 'delete role');

        RETURN NEXT is(@extschema@.delete_role('test_role'), 't', 'delete role');
        RETURN NEXT throws_ok('SELECT * FROM @extschema@.delete_role(''test_role'')', 'P0001', 'Role test_role not found');
END;
$$;

CREATE OR REPLACE FUNCTION delete_user(username text) RETURNS boolean AS $$
BEGIN
        PERFORM @extschema@._is_granted_for('delete', 'user');
       
        DELETE FROM @extschema@.users WHERE name=username;
        IF NOT found THEN
                RAISE EXCEPTION E'User % not found', username;
        END IF;
        PERFORM @extschema@._audit(format('Delete user %s', username));
        RETURN true;
END;
$$ LANGUAGE plpgsql;  
COMMENT ON FUNCTION delete_user(text) IS 'This command deletes an existing user from the RBAC database';

\unset ECHO

SELECT plan(1);

SELECT set_config("rbac.session_id", rbac.create_session('superuser'), true);
 
SELECT is(rbac.add_user('superuser', 'test'), 'f', 'double user');
SELECT is(rbac.add_role('rbac_schema', 'test'), 'f', 'double role');

SELECT is(rbac.add_role('test_role', 'test'), 't', 'add role');

SELECT is(rbac.assign_user('nouser', 'test_role'), 'f', 'no user');
SELECT is(rbac.assign_user('superuser', 'no_role'), 'f', 'no role');
SELECT is(rbac.assign_user('superuser', 'test_role'), 't', 'role assigned');
SELECT is(rbac.deassign_user('superuser', 'test_role'), 't', 'role deassigned');
SELECT is(rbac.deassign_user('superuser', 'test_role'), 'f', 'role already deassigned');

SELECT is(rbac.delete_role('test_role'), 't', 'delete role');
SELECT is(rbac.add_role('test_role', 'test role'), 'f', 'role not found');

SELECT is(rbac.grant_permission('view', 'permission', 'role1'), 'f', E'Role doesnt exists');
SELECT is(rbac.grant_permission('find', 'permission', 'rbac_schema'), 'f', E'Permission doesnt exists');
SELECT is(rbac.grant_permission('view', 'permission', 'rbac_schema'), 'f', E'Permission already granted');

SELECT is(rbac.grant_permission('view', 'permission', 'rbac_users'), 't', E'Permission granted');
SELECT is(rbac.revoke_permission('view', 'permission', 'role1'), 'f', E'Role doesnt exists');
SELECT is(rbac.revoke_permission('find', 'permission', 'rbac_schema'), 'f', E'Permission doesnt exists');
SELECT is(rbac.revoke_permission('view', 'permission', 'rbac_users'), 't', E'Permission revoked');
SELECT is(rbac.revoke_permission('view', 'permission', 'rbac_users'), 'f', E'Permission already revoked');

SELECT is(rbac.create_session('superuser', '{norole}'), NULL, '');
SELECT is(rbac.delete_session('superuser', '6091d2f2-bccb-454b-8ba8-6a47458b17ef'), true, '');
SELECT is(rbac.delete_session('superuser', rbac.create_session('superuser', '{rbac_schema}')), true, '');

SELECT * FROM finish();

SELECT FROM rbac.delete_session(SELECT current_setting('rbac.session_id'));
